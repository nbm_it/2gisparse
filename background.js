var params = {
        'emailMask': /\S+@\S+\.\S+/,
        'parsePhone': true,
        'mainLink': 'https://2gis.ru/spb',
        'contactPages': ['контакты', 'contacts', 'обратная связь'],
        'parseEmail': true,
        'parseNum': 50,
        'parseNumTolerance': 2,
        'parseSite': true,
        'phoneMask': /((\+7)|8|\([0-9])+([0-9-() ]+){7,18}/gim,
        'phrases': {
            'phrase-1': 'калибри'
        },
        'timeoutMax': 15000,
        'timeoutMaxTolerance': 2000,
        'timeout': 300,
        'timeoutTolerance': 20
    },
    indexId = false, status = 'STOP', cnsl = [], phrases = {}, urlAdd = '/search/', urlFinish = '/tab/firms', pageLink = '/page/', mainDomain = 'https://2gis.ru';

function setNewParams(prms) {
    for (var i in prms) {
        params[i] = prms[i];
    }
}

function toIndex(type, params) {
    chrome.tabs.sendMessage(indexId, {
        'from': 'background',
        'params': {
            'type': type,
            'data': params
        }
    });
}

function validate() {

    var success = 1, text = "";

    for (var i in params) {
        if ($.trim(params[i]).length < 1) {
            success = 0;
            text += " " + i + ": " + params[i] + ",";
        }
    }

    for (var i in params['phrases']) {
        if ($.trim(params['phrases'][i]).length < 1) {
            success = 0;
            text += " " + i + ": " + params['phrases'][i] + ",";
        }
    }

    if (text.length > 1) {
        text = text.substring(0, text.length - 1);
    }

    return {'success': success, 'text': text};
}

function start() {

    var valid = validate();

    if (!valid.success) {
        consol('error', 'Некоторые поля не заполнены, либо заполнены не верно:[ ' + valid.text + ' ]');
        return false;
    }

    for (var i in params.phrases) {
        setPhrases(i, params.phrases[i]);
    }

    status = 'START';
    toIndex('status', status);
    consol('status', 'Запуск парсинга');

    parsePhrases();

}

function setPhrases(id, phrase) {

    phrases[id] = {
        'name': phrase,
        'found': null,
        'ready': null,
        'errors': {},
        'page': 0,
        'pagesFound': 0,
        'pagePosition': 0,
        'blockPosition': 0,
        'params': {},
        'firmsOnPage': 12
    }

}

function stop() {

    status = 'STOP';
    toIndex('status', status);
    consol('status', 'Остановка парсинга');

}

function getTime(exp) {
    var date = new Date();
    var tm = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':' +
        (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':' +
        (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());

    return tm;
}

function rand(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function consol(type, str) {

    var time = getTime();
    var send = {
        'time': time,
        'type': type,
        'str': str
    };

    cnsl.push(send);

    toIndex('console', send);

}

chrome.browserAction.onClicked.addListener(function (activeTab) {
    chrome.tabs.create({url: '/index.html'}, function (tab) {
        indexId = tab.id;
    });
});

chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        if (request.from == 'index') {
            switch (request.params.type) {
                case 'setParams':
                    setNewParams(request.params.data)
                    break;
                case 'event':
                    if (request.params.data == 'start')
                        start();
                    else
                        stop();

                    break;
                case 'getOldParams':
                    toIndex('oldParams', params);
                    break;
                case 'getStatus':
                    toIndex('status', status);
                    break;
                case 'getConsole':
                    toIndex('oldConsole', cnsl);
                    break;
                case 'getPhrases':
                    toIndex('oldPhrases', phrases);
                    break;

            }
        }
    }
);

function parsePhrases() {

    var error = function (url, elem, err) {
        consol('error', 'Phrases error ' + url + ": " + err);
        elem.errors = {};
        elem.errors[getTime() + "|" + rand(11111111, 99999999)] = {
            text: "NOT PARSE " + url + ": " + err,
            url: url
        };
    };

    var success = function (elem, html) {
        elem.errors = false;
        setPhrasesMainParams(elem, html);
    };

    for (var i in phrases) {
        var url = params.mainLink + urlAdd + phrases[i].name + urlFinish;
        getPage(url, phrases[i], success, error);
    }

};

function parsePhrasesPages(elem) {

    var error = function (url, elem, err) {
        consol('error', 'Page error ' + url + ": " + err);
        elem.errors = {};
        elem.errors[getTime() + "|" + rand(11111111, 99999999)] = {
            text: "NOT PARSE " + url + ": " + err,
            url: url
        };
    };

    var success = function (elem, html) {
        setPhrasesPageParams(elem, html);
    };

    elem.page++;

    var url = params.mainLink + urlAdd + elem.name + pageLink + elem.page + urlFinish;

    if (elem.page == 1) {
        url = params.mainLink + urlAdd + elem.name + urlFinish;
    }

    elem.params[url] = {
        'firms': {},
        'errors': false
    };


    getPage(url, elem.params[url], success, error);

    if (elem.page < elem.pagesFound) {
        parsePhrasesPages(elem);
    }
}

function setPhrasesPageParams(elem, html) {

    var name = "", href = "", address = "", branches = {}, branchesUrl = "";

    html.find('.mixedResults__list._active .searchResults__list > article').map(function () {

        name = "", href = "", address = "", branches = {}, branchesUrl = "";

        name = $(this).find('a.miniCard__headerTitleLink').text();
        href = $(this).find('a.miniCard__headerTitleLink').attr('href');
        address = $(this).find('.miniCard__address').text();

        consol('def', 'Получение информациии о компании "' + name + '"');

        elem.firms[href] = {
            name: name,
            address: address,
            phones: {},
            emails: {},
            social: {},
            site: {},
            branches: branches,
            category: {},
            info: "",
            url: href,
            errors: false,
            other_contacts: {},
            from_site: {}
        };

        if ($(this).find('.miniCard__filialsWrapper').children('.link.miniCard__filials').length != 0) {
            branchesUrl = $(this).find('.miniCard__filialsWrapper').children('.link.miniCard__filials').attr('href');

            setBranches(mainDomain + branchesUrl, elem.firms[href]);

        }

        setFirms(mainDomain + href, elem.firms[href]);

    });
}

function setFirms(url, elem) {

    var error = function (url, elem, err) {
        consol('error', 'Branches error ' + url + ": " + err);
        elem.errors = {};
        elem.errors[getTime() + "|" + rand(11111111, 99999999)] = {
            text: "NOT PARSE " + url + ": " + err,
            url: url
        };
    };

    var success = function (elem, html) {
        var cont = 0, cats = 0, media = 0, socials = 0, oth = 0;

        media = html.find('div.mediaCard__main').length;

        if (media) {

            elem.info = html.find('.mediaAttributes__block').html();

            elem.category[0] = html.find('.mediaAttributes__groupTitle').html();

            html.find('.mediaContacts__group .mediaContacts__phonesNumber').map(function () {
                elem.phones[cont] = $(this).text();
                cont++;
            });

            elem.site = html.find('.mediaContacts .mediaContacts__website:first').html();

            elem.email = html.find('.mediaContacts__block .mediaContacts__email').html();

            html.find('.mediaCard__block .mediaContacts__socialItem').map(function () {
                elem.social[socials] = $(this).attr('href');
                socials++
            });

        } else {

            elem.info = html.find('.cardAttributes .cardAttributes__in').html();

            html.find('.link.cardRubrics__rubricLink').map(function () {
                elem.category[cats] = $(this).html();
                cats++;
            });

            html.find('.contact__phonesVisible .contact__phonesItemLinkNumber').map(function () {
                elem.phones[cont] = $(this).text();
                cont++;
            });

            elem.site = html.find('.contact__websites .contact__linkText:first').html();

            elem.email = html.find('.contact__link._type_email .contact__linkText').html();

            html.find('.contact__socials .contact__linkText').map(function () {
                elem.social[socials] = $(this).attr('href');
                socials++
            });

            $('.contact__otherList .contact__linkText').map(function () {
                elem.other_contacts[oth] = $(this).html();
                oth++;
            });

        }

        if (params.parseSite) {
            if (elem.site) {
                elem.from_site = {};
                getCompanySite('http://' + elem.site, elem);
            }
        }

        consol('def', 'Информация о компании  "' + elem.name + '" получена');

    };

    getPage(url, elem, success, error);

}

function getCompanySite(url, elem) {

    var error = function (url, elem, err) {
        consol('error', 'PARSE COMPANY SITE ERROR ' + url + ": " + err);
        elem.errors = {};
        elem.errors[getTime() + "|" + rand(11111111, 99999999)] = {
            text: "PARSE COMPANY SITE ERROR " + url + ": " + err,
            url: url
        };
    };

    var success = function (elem, html) {
        elem.errors = false;
        var pagesCont = false;

        consol('def', 'Начат сбор информации с ' + url);

        pagesCont = getContactsPage(elem, html);
        getContacts(elem, html);

        if (pagesCont) {
            if (pagesCont.indexOf('http://') == -1 && pagesCont.indexOf('https://') == -1) {
                pagesCont = 'http://' + pagesCont;
                pagesCont.replace('///', '//');
            }

            var sc = function (elem, html) {
                getContacts(elem, html);
            };

            var er = function (url, elem, err) {
                consol('error', 'PARSE COMPANY SITE (CONTACT PAGE) ERROR ' + url + ": " + err);
                elem.errors = {};
                elem.errors[getTime() + "|" + rand(11111111, 99999999)] = {
                    text: "PARSE COMPANY SITE ERROR " + url + ": " + err,
                    url: url
                };
            };

            getSite(pagesCont, elem, sc, er)
        }
    };

    getSite(url, elem, success, error)
}

function getContactsPage(elem, html) {

    var ct = false, url = elem.site, bbUrl = '';

    $(html).map(function () {
        if ($(this).find('a').length > 0) {
            $(this).find('a').map(function () {
                for (var i in params.contactPages) {
                    if ($(this).html().toLowerCase().indexOf(params.contactPages[i]) > -1) {
                        //if (params.contactPages[i] == $.trim($(this).html()).toLowerCase()) {

                        if ($(this).attr('href').indexOf('://') + 1) {
                            ct = $(this).attr('href');
                        } else {
                            bbUrl = url.replace('WWW', '').replace('www', '').replace('http://', '').replace('https://', '');
                            if ($(this).attr('href').indexOf(bbUrl) > -1) {
                                ct = $(this).attr('href');
                            } else {
                                if ($(this).attr('href')[0] == '/')
                                    ct = url + $(this).attr('href');
                                else
                                    ct = url + '/' + $(this).attr('href');
                            }
                        }
                    }
                }
            });
        }
    });

    return ct;
}

function getContacts(elem, html) {

    var txt = $(html).text();
    txt = txt.replace(/ /g, '').replace(/<[^>]+>/g, '').replace(/\s{2,}/g, ' ');

    var phone = txt.match(params.phoneMask);
    var email = txt.match(params.emailMask);

    if (params.parseEmail) {
        elem.from_site.emails = [];
        for (var i in email) {
            if (i <= email.length) {
                if (elem.from_site.emails.indexOf($.trim(email[i])) == -1) {
                    elem.from_site.emails.push($.trim(email[i]));
                }
            }
        }
    }

    if (params.parsePhone) {
        elem.from_site.phones = []
        for (var i in phone) {
            if (i <= phone.length) {
                if (elem.from_site.phones.indexOf($.trim(phone[i])) == -1) {
                    elem.from_site.phones.push($.trim(phone[i]));
                }
            }
        }
    }
}

function getSite(url, elem, success, error) {
    rnd = rand(11111, 99999);
    $.ajax({
        url: url,
        type: 'GET',
        success: function (data) {
            success(elem, data);
        },
        fail: function (jqXHR, textStatus) {
            error(url, elem, "Request failed: " + textStatus);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            error(url, elem, "Request failed: " + textStatus);
        }
    });
}

/*
 function getParamsFromSite(url, elem) {

 elem.from_site = {};

 var error = function (url, elem, err) {
 consol('error', 'Company site error ' + url + ": " + err);
 elem.errors = {};
 elem.errors[getTime() + "|" + rand(11111111, 99999999)] = {
 text: "NOT PARSE " + url + ": " + err,
 url: url
 };
 };

 var success = function (elem, html) {
 parseSiteCompany(elem, html);
 var ct = getContactPages(html);

 if (ct)
 getPageSite(url, elem.from_site, function (elem, html) {
 parseSiteCompany(elem, html);
 }, error);

 };

 getPageSite(url, elem.from_site, success, error);
 }

 function getContactPages(html) {
 var ct = false;
 $(html).map(function () {
 if ($(this).find('a').length > 0) {
 $(this).find('a').map(function () {
 for (var i in params.contactPages) {
 if (params.contactPages[i] == $.trim($(this).html()).toLowerCase()) {
 if ($(this).attr('href').indexOf('://') + 1) {
 ct = $(this).attr('href');
 } else {
 ct = url + $(this).attr('href');
 }
 }
 }
 });
 }
 });

 return ct;
 }

 function getPageSite(url, elem, success, error) {

 console.log('PARSE ' + url)

 $.ajax({
 url: url,
 type: 'GET',
 success: function (data) {

 console.log('PARSE SUCCESS')

 parseSiteCompany(elem, data);

 },
 fail: function (jqXHR, textStatus) {
 error(url, elem, "Request failed: " + textStatus);
 },
 error: function (jqXHR, textStatus, errorThrown) {
 error(url, elem, "Request failed: " + textStatus);
 }
 })
 }

 function parseSiteCompany(elem, html) {

 console.log('STR')

 var txt = $(html).text()
 txt = txt.replace(/\s{2,}/g, ' ');
 var phone = txt.match(params.emailMask);
 var email = txt.match(params.phoneMask);

 if (params.parseEmail) {
 elem.emails = email;
 }

 if (params.parsePhone) {
 elem.phones = phone;
 }

 console.log(elem.emails);
 console.log(elem.phones);
 }*/

function setBranches(urlDef, elem) {

    var url = urlDef.split('?');

    if (url.length > 1)
        url = url[0];
    else
        url = urlDef;

    var error = function (url, elem, err) {
        consol('error', 'Branches error ' + url + ": " + err);
        elem.branches.errors = {};
        elem.branches.errors[getTime() + "|" + rand(11111111, 99999999)] = "NOT PARSE " + url + ": " + err;
    };

    var success = function (elem, html) {
        elem.branches = {};
        elem.branches.list = {};

        var i = 0;

        html.find('.searchResults__clipper .searchResults__content').find('article').map(function () {
            i++;
            elem.branches.list[i] = $(this).find('.miniCard__headerTitleLink').html();

        });

        consol('def', 'Филиалы компании "' + elem.name + '" получены.');

    };

    getPage(url, elem, success, error);
}


function setPhrasesMainParams(elem, html) {
    var numbers = 0;

    if (html) {
        numbers = html.find('.searchResults__headerName').html().split(' ');
        elem.found = parseInt(numbers[0]);
        elem.firmsOnPage = html.find('.mixedResults__list._active .searchResults__list > article').length;
        elem.pagesFound = Math.ceil(elem.found / elem.firmsOnPage);
    }

    consol('def', 'Получена основная информация по фразе ' + elem.name);

    parsePhrasesPages(elem);
}

var intervl = 0
// var rnd = 0;
// var rnd2 = 0;
// var sv_start = 0;

// function sv() {
//
//     setTimeout(function () {
//
//         console.log(rnd + " == " + rnd2);
//
//         if (rnd == rnd2) {
//             console.log('Записать')
//             sv_start = 0;
//         } else {
//             sv_start = 1;
//             rnd2 = rnd;
//             sv();
//         }
//
//     }, parseInt(params.timeout) + 10);
//
// }


var intervalServer = 0;

function saveParams() {

    for (var i in phrases) {
        for (var i2 in phrases[i]['params']) {
            for (var i3 in phrases[i]['params'][i2]['firms']) {
                saveToServer(phrases[i]['params'][i2]['firms'][i3]);
            }
        }
    }

    intervalServer = 0;
}

function saveToServer(elem) {
    setTimeout(function () {
        $.ajax({
            type: 'POST',
            url: 'http://bpx3.ru/2gisparse.php',
            data: elem,
            success: function () {
                consol('def', 'Информация о компании "' + elem.name + '" добавлена');
            }
        });
    }, intervalServer += 300);
}

function getPage(url, elem, success, error) {

    // rnd = rand(11111, 99999);
    // if(sv_start == 0){
    //     sv();
    // }

    intervl += parseInt(params.timeout);
    console.log(intervl)
    setTimeout(function () {
        $.ajax({
            url: url,
            type: 'GET',
            success: function (dt) {

                console.log(elem.name)

                var html = $.parseHTML(dt);
                var parseHtml = false;
                for (var i in html) {
                    if ($(html[i]).hasClass('layout__content')) {
                        parseHtml = $(html[i]);
                    }
                }

                success(elem, parseHtml);

            },
            fail: function (jqXHR, textStatus) {
                error(url, elem, "Request failed: " + textStatus);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                error(url, elem, "Request failed: " + textStatus);
            }
        })
    }, intervl)
}

//
// var intervalId, indexId = false,
//     thParsePageId = false,
//     THPARSEGOODSPAGE = false;
// THPARSEGOOD = false;
// typParse = 'segments',
//     intervarlTime = 10000,
//     url = 'http://www.sumki-platki.ru',
//     addtoUrl = '?num=300';
// notParse = {
//     0: '/catalog/',
// };
// nav = {},
//     navComp = false,
//     goods = {},
//     goodsComp = false;
//
// var sParams = {
//     'name': 0,
//     'country': 0,
//     'price': 0,
//     'discount_price': 0,
//     'discount_percentage': 0,
//     'description': 0,
//     'brand': 0,
//     'size1': 0,
//     'size2': 0,
//     'size3': 0,
//     'size4': 0,
//     'category': 0,
//     'subcategory': 0,
//     'section': 0,
//     'color': 0,
//     'material': 0,
//     'weight': 0,
//     'diameter_dome': 0,
//     'diameter_folded_umbrella': 0,
//     'number_spokes': 0,
//     'mechanism': 0,
//     'site_material': 0,
//     'images': 0,
//     'sizes': 0,
//     'url': 0
// }
//
//
// function saveParams(p, funcDone, funcFail) {
//     console.log('saveParams');
//     console.log(p);
//     $.ajax({
//         url: "http://bpx3.ru/parsesumkipaltki/addtest.php",
//         type: "post",
//         data: {
//             'name': p.name,
//             'country': p.country,
//             'price': p.price,
//             'discount_price': p.discount_price,
//             'discount_percentage': p.discount_percentage,
//             'description': p.description,
//             'brand': p.brand,
//             'size1': p.size1,
//             'size2': p.size2,
//             'size3': p.size3,
//             'size4': p.size4,
//             'category': p.category,
//             'subcategory': p.subcategory,
//             'section': p.section,
//             'color': p.color,
//             'material': p.material,
//             'weight': p.weight,
//             'diameter_dome': p.diameter_dome,
//             'diameter_folded_umbrella': p.diameter_folded_umbrella,
//             'number_spokes': p.number_spokes,
//             'mechanism': p.mechanism,
//             'site_material': p.site_material,
//             'images': p.images,
//             'sizes': p.sizes,
//             'url': p.url,
//         }
//     }).done(function (data) {
//         console.log(data);
//         if (data.error == false) {
//             funcDone(data);
//         } else {
//             funcFail(data);
//         }
//     }).fail(function (data) {
//         console.log(data);
//         funcFail(data);
//     });
// };
//
// function saveImg(urlm, funcDone, funcFail) {
//     console.log('saveImg');
//     console.log(url);
//     $.ajax({
//         url: "http://bpx3.ru/parsesumkipaltki/addimages.php",
//         type: "post",
//         data: {
//             'url': url + urlm
//         }
//     }).done(function (data) {
//         console.log(data);
//         if (data.error == false) {
//             funcDone(data);
//         } else {
//             funcFail(data);
//         }
//     }).fail(function (data) {
//         console.log(data);
//         funcFail(data);
//     });
// };
//
//
// chrome.runtime.onMessage.addListener(
//     function (request, sender, sendResponse) {
//
//         if (request.from == 'index') {
//             switch (request.params) {
//                 case 'start':
//                     console.log("STARTED");
//                     start();
//                     chrome.tabs.sendMessage(indexId, {
//                         'from': 'background',
//                         'params': 'STARTED'
//                     });
//                     break;
//                 case 'stop':
//                     console.log("STOPPED");
//                     stop();
//                     chrome.tabs.sendMessage(indexId, {
//                         'from': 'background',
//                         'params': 'STOPPED'
//                     });
//                     break;
//             }
//         } else if (request.from == 'content') {
//             switch (request.type) {
//                 case 'who':
//                     chrome.tabs.sendMessage(thParsePageId, {
//                         'from': 'background',
//                         'params': typParse,
//                         'THPARSEGOODSPAGE': THPARSEGOODSPAGE,
//                         'THPARSEGOOD': THPARSEGOOD
//                     });
//                     break;
//
//                 case 'parseGoods':
//
//                     saveParseGoodsPage(request);
//                     chrome.tabs.remove(thParsePageId);
//
//                     break;
//
//                 case 'parseGood':
//
//                     saveParseGood(request);
//                     chrome.tabs.remove(thParsePageId);
//
//                     break;
//
//                 case 'sections':
//
//                     nav = request.resSections;
//                     navComp = true;
//
//                     chrome.tabs.sendMessage(indexId, {
//                         'from': 'background',
//                         'params': "GET NAV"
//                     });
//
//                     var str = "NAV RESULT: <div class='nav'>";
//
//                     for (var k in nav) {
//                         str += '<div class="section">';
//                         str += k + ' (' + validarra(nav[k]) + ')<br/>';
//                         for (var k2 in nav[k]) {
//                             str += '-' + nav[k][k2]['name'] + ' [' + nav[k][k2]['url'] + '] (' + validarra(nav[k][k2]['podpodSection']) + ')<br/>';
//                             for (var k3 in nav[k][k2]['podpodSection']) {
//                                 str += '--' + nav[k][k2]['podpodSection'][k3]['name'] + ' [' + nav[k][k2]['url'] + ']<br/>';
//                             }
//                         }
//                         str += '</div>';
//                     }
//
//                     str += "</div>";
//
//                     chrome.tabs.sendMessage(indexId, {
//                         'from': 'background',
//                         'params': str
//                     });
//
//                     chrome.tabs.remove(thParsePageId);
//
//                     parsePage();
//
//                     break;
//
//             }
//         }
//     }
// );
//
// chrome.browserAction.onClicked.addListener(function (activeTab) {
//     chrome.tabs.create({url: '/index.html'}, function (tab) {
//         indexId = tab.id;
//     });
// });
//
// function start() {
//
//     chrome.tabs.sendMessage(indexId, {
//         'from': 'background',
//         'params': 'Start parse nav'
//     });
//
//     parseNav();
//
// }
//
// function parseNav() {
//     chrome.tabs.create({url: url}, function (tab) {
//         thParsePageId = tab.id;
//     });
// }
//
// function validarra(arr) {
//     return Object.keys(arr).length;
// }
//
// varAllParsePages = {};
//
// function parsePage() {
//     chrome.tabs.sendMessage(indexId, {
//         'from': 'background',
//         'params': 'Start parse pages'
//     });
//
//     var z = 0;
//
//     for (var k in nav) {
// //Сегмент
//         for (var k2 in nav[k]) {
// //Категории
//             if (validarra(nav[k][k2]['podpodSection']) > 0) {
//                 for (var k3 in nav[k][k2]['podpodSection']) {
// //Подкатегории
//
//                     if (!validUrl(nav[k][k2]['podpodSection'][k3]['url'])) {
//                         continue;
//                     }
//
//                     varAllParsePages[z] = {
//                         'parse': false,
//                         'segment': k,
//                         'category': nav[k][k2]['name'],
//                         'podcategory': nav[k][k2]['podpodSection'][k3]['name'],
//                         'url': nav[k][k2]['podpodSection'][k3]['url']
//                     }
//                     z++;
//                 }
//             } else {
//
//                 if (!validUrl(nav[k][k2]['url'])) {
//                     continue;
//                 }
//
//                 varAllParsePages[z] = {
//                     'parse': false,
//                     'segment': k,
//                     'category': nav[k][k2]['name'],
//                     'podcategory': '',
//                     'url': nav[k][k2]['url']
//                 }
//                 z++;
//             }
//         }
//     }
//     console.log(varAllParsePages)
//     goGoodsParse();
//
// }
//
// function goGoodsParse() {
//
//     var thIdParse = false;
//     var thUrlParse = false;
//     typParse = 'parseGoods';
//
//     for (var i in varAllParsePages) {
//         if (varAllParsePages[i]['parse'] == false) {
//             thIdParse = i;
//             thUrlParse = varAllParsePages[i]['url'];
//             break;
//         }
//     }
//
//     if (thIdParse) {
//         THPARSEGOODSPAGE = thIdParse;
//
//         chrome.tabs.sendMessage(indexId, {
//             'from': 'background',
//             'params': 'Start parse ' + thIdParse + ' ' + url + thUrlParse + addtoUrl
//         });
//
//         chrome.tabs.create({url: url + thUrlParse + addtoUrl}, function (tab) {
//             thParsePageId = tab.id;
//         });
//     } else {
//         console.log(ParseGoodsPage);
//         ParseGood();
//         chrome.tabs.sendMessage(indexId, {
//             'from': 'background',
//             'params': 'Finish parse goods pages [FOUND GOODS: ' + Object.keys(ParseGoodsPage).length + ']'
//         });
//     }
// }
//
// var ParseGoodsPage = {};
//
// function saveParseGoodsPage(r) {
//     varAllParsePages[THPARSEGOODSPAGE]['parse'] = true;
//     var s = 0;
//     for (var i in r.resGoods) {
//         ParseGoodsPage[THPARSEGOODSPAGE + '_' + i] = {
//             'parse': false,
//             'segment': varAllParsePages[THPARSEGOODSPAGE]['segment'],
//             'category': varAllParsePages[THPARSEGOODSPAGE]['category'],
//             'podcategory': varAllParsePages[THPARSEGOODSPAGE]['podcategory'],
//             'podcategory_url': varAllParsePages[THPARSEGOODSPAGE]['url'],
//             'good_url': r.resGoods[i],
//         };
//         s++;
//     }
//
//     chrome.tabs.sendMessage(indexId, {
//         'from': 'background',
//         'params': 'Finish parse ' + THPARSEGOODSPAGE + ' ' + url + varAllParsePages[THPARSEGOODSPAGE]['url'] + addtoUrl + ' [FOUND GOODS: ' + s + ']'
//     });
//
//     goGoodsParse();
//
// }
//
// function ParseGood() {
//
//     typParse = 'parseGood';
//     var thIdParse = false;
//     var thUrlParse = false
//
//     for (var ob in ParseGoodsPage) {
//         if (ParseGoodsPage[ob]['parse'] == false) {
//             thIdParse = ob;
//             thUrlParse = ParseGoodsPage[thIdParse]['good_url'];
//             break;
//         }
//     }
//
//     if (thIdParse) {
//         THPARSEGOOD = thIdParse;
//
//         chrome.tabs.sendMessage(indexId, {
//             'from': 'background',
//             'params': 'Start parse ' + thIdParse + ' ' + url + thUrlParse
//         });
//
//         chrome.tabs.create({url: url + thUrlParse}, function (tab) {
//             thParsePageId = tab.id;
//         });
//
//     } else {
//         console.log(ParseGoodsPage);
//         chrome.tabs.sendMessage(indexId, {
//             'from': 'background',
//             'params': 'Finish parse good pages [FOUND GOODS: ' + Object.keys(ParseGoodsPage).length + ']'
//         });
//     }
// }
//
// function saveParseGood(r) {
//
//     ParseGoodsPage[r.THPARSEGOOD]['parse'] = true;
//
//     var sks = ParseGoodsPage[r.THPARSEGOOD];
//     var res = r.resGood;
//     var ResFinish = {};
//
//     for (let i in sParams) {
//         if (i in res) {
//             ResFinish[i] = res[i];
//         } else {
//             ResFinish[i] = sParams[i];
//         }
//     }
//     ;
//
//     ResFinish['url'] = sks['good_url'];
//     ResFinish['section'] = sks['segment'];
//     ResFinish['category'] = sks['category'];
//     ResFinish['subcategory'] = sks['podcategory'];
//
//     saveInBD(ResFinish, function () {
//         ParseGood();
//     });
// }
//
// var imagesIn = {};
// var imgStr = "";
// var THSAVEINDB = false;
// var onTHSFSFSf = false;
//
// function saveInBD(res, onCompl) {
//
//     imagesIn = {};
//     imgStr = "";
//
//     for (let i in res['images']) {
//         imagesIn[i] = {
//             'parse': false,
//             'url': res['images'][i]
//         }
//     }
//
//     THSAVEINDB = res;
//     onTHSFSFSf = onCompl;
//
//     svImg();
// }
//
// function svImg() {
//     var z = getnxtImg();
//
//     if (z !== false) {
//         saveImg(z, function (dt) {
//
//             dt = JSON.parse(dt);
//             console.log(dt);
//
//             if (dt.error == false) {
//                 imgStr += dt.dir + '|||';
//             }
//
//             svImg();
//         }, function (dt) {
//
//             dt = JSON.parse(dt);
//             console.log(dt);
//
//             if (dt.error == false) {
//                 imgStr += dt.dir + '|||';
//             }
//
//             svImg();
//         });
//     } else {
//         THSAVEINDB['images'] = imgStr;
//         saveParams(THSAVEINDB, function () {
//             onTHSFSFSf();
//         }, function () {
//             onTHSFSFSf();
//         });
//     }
// }
//
// function getnxtImg() {
//     var t = 0;
//     for (let i in imagesIn) {
//         t++;
//     }
//
//     if (!t) {
//         return false;
//     }
//
//     var b = false;
//     for (let i in imagesIn) {
//         if (imagesIn[i]['parse'] == false) {
//             imagesIn[i]['parse'] = true;
//             return imagesIn[i]['url'];
//         }
//     }
//
//     return b;
// }
//
// function validUrl(url) {
//
//     for (var i in notParse) {
//         if (notParse[i] == url) {
//             return false;
//         }
//     }
//     return true;
// }
//
// function stop() {
//     clearInterval(intervalId);
// }
