/*function onBlur() { // окно теряет фокус
	chrome.runtime.sendMessage({site:sait,time:localStorage[sait]}); // отправка сообщения на background.js
	localStorage[sait] = '0';	
}
    window.onblur = onBlur; // если окно теряет фокус
	function sec() //выполняется каждую секунду
	{ 
	  if(document.webkitVisibilityState == 'visible')//если страница активна
	    {
		   localStorage[sait] =  parseInt(localStorage[sait],10) +1; // обновляем данные о сайте в локальном хранилище
	    }
	}			 	
var sait=location.hostname; // на каком сайте находится скрипт
localStorage[sait] = '0';
setInterval(sec, 1000);// запускать функцию каждую секунду*/

chrome.runtime.sendMessage({
	'from': 'content',
	'type': 'who'
});

THPARSEGOODSPAGE = false;
THPARSEGOOD = false;

chrome.runtime.onMessage.addListener(
	function(request, sender, sendResponse) {
		if(request.from == 'background'){
			
			switch(request.params){
				case('segments'):
					getSegments();
				break;
				case('parseGoods'):
					THPARSEGOODSPAGE = request.THPARSEGOODSPAGE;
					parseGoods();
				break;
				case('parseGood'):
					THPARSEGOOD = request.THPARSEGOOD;
					parseGood();
				break;
			}
			
			
		}
	}
);

function getSegments(){
	var resSections = {};

	$('.content nav').map(function(){
		
		var nameSection = $(this).prevAll( 'p:first' ).text();
		if($.trim(nameSection).length < 1){
			return false;
		}
		
		var podSection = {};
		var i = 0;
		
		$(this).children('div').children('a').map(function(){
			
			podSection[i] = {};
			
			var nameRazdelPre = $(this).html().split('<');
			
			podSection[i]['name'] = nameRazdelPre[0];
			podSection[i]['url'] = $(this).attr('href');
			podSection[i]['podpodSection'] = {};

			var i2 = 0;
			
			$(this).parent('div').children('ul').find('li').map(function(){
				podSection[i]['podpodSection'][i2] = {
					'name': $(this).children('a').html(),
					'url': $(this).children('a').attr('href')
				}
				i2++;
			});
			
			i++;
		})
		
		resSections[nameSection] = podSection;
		
	});

	chrome.runtime.sendMessage({
		'from': 'content',
		'type': 'sections',
		'resSections': resSections
	});
}

function parseGoods(){
	var resGoods = {}, i = 0;
	
	$('.line_lfb.th_lin .h_lsq').map(function(){
		resGoods[i] = $(this).attr('href');
		i++;
	});
	
	chrome.runtime.sendMessage({
		'from': 'content',
		'type': 'parseGoods',
		'THPARSEGOODSPAGE': THPARSEGOODSPAGE,
		'resGoods': resGoods
	});
}

function parseGood(){
	var resGood = {
		'name': $('.mainbar h1').html(),
		'country': $('.mainbar .element-country').html(),
		'description': $('.container_tabcontrol .some_part_tabcontrol:eq(0)').html(),
		'brand': $('.container_tabcontrol .some_part_tabcontrol:eq(1)').html(),
	}
	
	var imgs = [];
	$('.mainbar .element-left-col').find('a.element-icons').map(function(){
		imgs[imgs.length] = $(this).attr('href');
	});
	resGood['images'] = imgs;
	
	if($('.mainbar s').is('.oldprice')) {
		resGood['price'] = $('.oldprice').html().replace("руб.","").replace(/\s+/g,'');
		resGood['discount_price'] = $('span.price').html().replace("руб.","").replace(/\s+/g,'');
	}else{
		resGood['price'] = $('span.price').html().replace("руб.","").replace(/\s+/g,'');
		resGood['discount_price'] = resGood['price'];
	}
	
	var sootvSiteSaveParams = {
		'Цвет:': 'color',
		'Материал:': 'material',
		'Вес:': 'weight',
		'Механизм:': 'mechanism',
		'Диаметр купола:': 'diameter_dome',
		'Длина сложенного зонта:': 'diameter_folded_umbrella',
		'Количество спиц:': 'number_spokes',
		'Выбрать размер:': 'sizes',
		'Материал подкладки:': 'site_material',
	}
	var ParamsRes = {};
	
	$('.mainbar .element-right-col .params').map(function(){
		var k = $(this).html().split('<b>'), arr = [], arrRes = {};
		for(var b in k){
			var z = k[b].split('</b>');
			for(var br in z){
				arr[arr.length] = $.trim(z[br].replace(/<\/?[^>]+>/g,''));
			}
		}
		delete arr[0];
		var a = 0;
		for(var t in arr){
			a++;
			if ( a & 1 ) {
				arrRes[arr[t]] = arr[a+1];
			}
		}
		
		for(var i in arrRes){
			ParamsRes[sootvSiteSaveParams[i]] = arrRes[i];
			if(sootvSiteSaveParams[i] == 'weight'){
				ParamsRes[sootvSiteSaveParams[i]] = ParamsRes[sootvSiteSaveParams[i]].replace(' г', '');
				if(ParamsRes[sootvSiteSaveParams[i]] == 'г'){
					ParamsRes[sootvSiteSaveParams[i]] = 0;
				}
			}else if(sootvSiteSaveParams[i] == 'sizes'){
				var srt = "";
				$('.mainbar .element-right-col .params .sizes span').map(function(){
					if(srt.length < 1){
						srt += $(this).text();
					}else{
						srt += '/'+$(this).text();
					}
				});
				ParamsRes[sootvSiteSaveParams[i]] = srt;
			}
		}
	});
	
	for(var brz in ParamsRes){
		resGood[brz] = ParamsRes[brz];
	}
	
	if($('img').is('.discount')){
		var b = $('img.discount').attr('src');
		var z = b.split('/');
		var k = z[z.length-1];
		var res = k.split('.');
		resGood['discount_percentage'] = res[0];
	}else{
		resGood['discount_percentage'] = 0;
	}
	
	
	if($('div').is('.element-dimensions')){
		var i = 1;
		$('.element-dimensions').find('div').map(function(){
			resGood['size'+i] = $.trim($(this).html().replace(' см', ''));
			i++;
		});
	}
	
	console.log(resGood)
	
	chrome.runtime.sendMessage({
		'from': 'content',
		'type': 'parseGood',
		'THPARSEGOOD': THPARSEGOOD,
		'resGood': resGood
	});
}
