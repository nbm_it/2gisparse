var status = 'STOP';

$('.addPhrase').click(function () {
    var parent = $(this).parents('.left-in');
    var parentAdd = $(this).parents('.input-block');
    var obj = parent.find('.input-block.phrases:first');
    var newObj = obj.clone();
    newObj.find('input').val('')
    $(parentAdd).before(newObj);
    refreshPhrases();
});

setDel();

function setDel() {

    if ($('.input-block.phrases').length < 2)
        $('.delPhrase').css('display', 'none');
    else
        $('.delPhrase').css('display', 'inline-block');

    $('.delPhrase').click(function () {
        $(this).parents('.input-block.phrases').remove();
        refreshPhrases();
    });
}

getOldParams();
getStatus();
getConsole();

function getOldParams() {
    toBk('getOldParams', false);
}

function getStatus() {
    toBk('getStatus', false);
}

function getConsole() {
    toBk('getConsole', false);
}

function setOldParams(params) {
    for (var i in params) {

        if (i != 'phrases') {
            setValue(i, params[i]);
        } else {
            var z = 1, iz = 0;

            $('.input-block.phrases').map(function () {
                iz++;

                if (iz > 1) {
                    $(this).remove();
                }

            });

            for (var p in params[i]) {
                if (z > 1) {
                    $('.addPhrase').click();
                }
                z++;
                setValue(p, params[i][p]);

            }
        }

    }
}

function getOldPhrases() {
    toBk('getPhrases', false);
}

function setOldPhrases(params) {

    console.log(params);
    var txt = "", percent = 0;
    $('.phrases-status').html('');

    for (var i in params) {
        if ($.trim(params[i]).length > 1) {


            percent = getPercent(params[i].found, params[i].ready);

            txt = "<div class='phrase-status-block phrase-" + i + "'>" +
                "<div class='phrase-top'>" +
                "<div class='phrase-name'>" + params[i].name + "</div>" +
                "<div class='phrase-info'>" +
                "<div class='phrase-found'>Найдено: " + params[i].found + "</div>" +
                "<div class='phrase-ready'>Завершено: " + params[i].ready + "</div>" +
                "<div class='phrase-errors'>Ошибок: " + params[i].errors + "</div>" +
                "</div>" +
                "</div>" +
                "<div class='phrase-bottom'>" +
                "<div class='phrase-percent'>" +
                "<div class='phrase-percent-number'>" +
                percent + "%" +
                "</div>" +
                "<div class='phrase-percent-ready' style='width:" + percent + "%;" + "'></div>" +
                "</div>" +
                "</div>" +
                "</div>";

            $('.phrases-status').append(txt);
        }
    }
}

function getPercent(all, ready) {

    if (!all || !ready)
        return 0;

    var one = all / 100;
    var rdy = ready / one;

    return Math.floor(rdy);
}

function setOldConsole(cnsl) {

    for (var i in cnsl) {
        addToConsole(cnsl[i].time, cnsl[i].type, cnsl[i].str);
    }
}

function addToConsole(time, type, str) {

    var strRes = '<div class="console-line ' + type + '">' +
        '<span class="console-time">'
        + time +
        '</span>' +
        '<span class="console-type">'
        + type +
        '</span>' +
        '<span class="console-content">'
        + str +
        '</span>' +
        '</div>';

    $('.console').prepend(strRes);
}

function setValue(name, value) {

    var type = $("#" + name).attr('type');
    if (type == 'checkbox') {
        if (value)
            $("#" + name).attr('checked', true).val(1);
        else
            $("#" + name).attr('checked', false).val(0);
    } else {
        if (name == 'contactPages')
            $("#" + name).val(value.join());
        else
            $("#" + name).val(value);

    }

}

function setStatus(params) {
    status = params;
    if (params == 'STOP') {
        $("#start").show();
        $("#stop").hide();
        getOldPhrases();
    } else if (params == 'START') {
        $("#start").hide();
        $("#stop").show();
        getOldPhrases();
    }
}

function refreshPhrases() {
    var i = 1, label, input;

    $('.input-block.phrases').map(function () {
        label = $(this).find('label');
        input = $(this).find('input');
        label.html('Фраза ' + i);
        label.attr('for', 'phrase-' + i);
        input.attr('id', 'phrase-' + i);
        input.attr('name', 'phrase-' + i);
        i++;
    });

    setDel();
}

$('.setOptions').click(function () {

    if (status != 'STOP') {
        alert('Перед применением изменений необходимо остановить парсинг!');
        return false;
    }

    var ph = getPhrases();
    var set1 = getSet1();
    var set2 = getSet2();
    var set3 = getSet3();
    var resultObject = $.extend({}, set1, set2, set3);
    resultObject['phrases'] = ph;

    toBk('setParams', resultObject);
});

function getSet1() {
    var set1 = {};
    set1.parseSite = $('#parseSite').is(':checked');
    set1.getPhone = $('#getPhone').is(':checked');
    set1.parseEmail = $('#parseEmail').is(':checked');

    return set1;
}

function getSet2() {
    var set2 = {};

    set2.contactPages = [];

    if ($('#contactPages').val().length > 1) {
        set2.contactPages = $('#contactPages').val().split(',');
    }

    if (!$('#phoneMask').attr('disabled'))
        set2.phoneMask = $('#phoneMask').val();


    if (!$('#emailMask').attr('disabled'))
        set2.phoneMask = $('#emailMask').val();

    return set2;
}

function getSet3() {
    var set3 = {};
    set3.mainLink = $('#mainLink').val();
    set3.timeout = $('#timeout').val();
    set3.timeoutTolerance = $('#timeoutTolerance').val();
    set3.timeoutMax = $('#timeoutMax').val();
    set3.timeoutMaxTolerance = $('#timeoutMaxTolerance').val();
    set3.parseNum = $('#parseNum').val();
    set3.parseNumTolerance = $('#parseNumTolerance').val();

    return set3;
}


function getPhrases() {
    var phrases = {}, id;

    $('.inpPh').map(function () {
        id = $(this).attr('id');
        if ($.trim($(this).val()).length > 1) {
            phrases[id] = $(this).val();
        }
    });

    return phrases;
}

$("#start").click(function () {
    $('.setOptions').click();
    toBk('event', 'start');
});

$("#stop").click(function () {
    var result = confirm('Вся статистика обнулится, парсинг не сможет быть продолжен. Вы согласны?');
    if (!result)
        return false;

    toBk('event', 'stop');
});

function toBk(type, params) {
    chrome.runtime.sendMessage({
        'from': 'index',
        'params': {
            'type': type,
            'data': params
        }
    })
}

chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        if (request.from == 'background') {

            switch (request.params.type) {
                case 'oldParams':
                    setOldParams(request.params.data);
                    break;
                case 'status':
                    setStatus(request.params.data);
                    break;
                case 'console':
                    addToConsole(request.params.data.time, request.params.data.type, request.params.data.str);
                    break;
                case 'oldConsole':
                    setOldConsole(request.params.data);
                    break;
                case 'oldPhrases':
                    setOldPhrases(request.params.data);
                    break;
            }

        }
    }
);

// chrome.runtime.onMessage.addListener(
//     function (request, sender, sendResponse) {
//         var date = new Date();
//         var tm = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':' +
//             (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':' +
//             (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
//
//         if (request.from == 'background') {
//             $('.content').prepend('<div class="infoBlock"><span>' + tm + '</span> | ' + request.params + '</div>');
//         }
//     }
// );
//
// $("#start").click(function () {
//     chrome.runtime.sendMessage({
//         'from': 'index',
//         'params': 'start'
//     });
//
//     $("#start").hide();
//     $("#stop").show();
// });
//
// $("#stop").click(function () {
//     chrome.runtime.sendMessage({
//         'from': 'index',
//         'params': 'stop'
//     })
//     $("#start").show();
//     $("#stop").hide();
// });
//
// $("#stop").hide();